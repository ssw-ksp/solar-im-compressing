id: fetchdata
label: fetch_data
class: CommandLineTool
cwlVersion: v1.2
inputs: 
  - id: surl_link
    type: string
    inputBinding:
      position: 0
outputs: 
  - id: solution
    type: File
    outputBinding:
      glob: '*.h5'
baseCommand: 
 - 'python3'
 - 'fetch.py'
doc: 'Untar a compressed file'
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'fetch.py' 
        entry: |
          import sys
          import requests
          import logging
          url = sys.argv[1]
          # srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lt16_001/888554/11/solutions.h5
          sas_id, task_id, filename = url.replace("srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lt16_001/", "").split("/")
          base_url = "https://spaceweather.astron.nl/SolarKSP/data/atdb_process/solar_calibrator_imaging"
          full_url = f"{base_url}/{task_id}/{sas_id}/solutions/{filename}"

          response = requests.get(full_url)
          if response.ok:
            with open(filename, "wb") as fout:
              fout.write(response.content)
          else:
            logging.error("%s", response.content)
            raise SystemExit(response.status_code)

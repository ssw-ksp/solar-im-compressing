#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool
label: unflag

requirements:
- class: InitialWorkDirRequirement
  listing:
  - writable: true
    entry: $(inputs.msin)
  - entryname: script.sh
    entry: |
      #!/bin/bash

      taql update $(inputs.msin.basename) set FLAG_ROW=false
      taql update $(inputs.msin.basename) set WEIGHT_SPECTRUM=1

inputs:
- id: msin
  type: Directory

outputs:
- id: msout
  type: Directory
  outputBinding:
    glob: $(inputs.msin.basename)

baseCommand: bash
arguments:
- script.sh

hints:
- class: DockerRequirement
  dockerPull: astronrd/linc:latest
- class: ResourceRequirement
  coresMin: 5

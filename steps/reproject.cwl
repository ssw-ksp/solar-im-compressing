#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: NetworkAccess
  networkAccess: true

inputs:
- id: fits_files
  type: File[]
  inputBinding:
    position: 1

outputs:
- id: preview_video
  type: File
  outputBinding:
    glob: out/*.mp4
- id: reprojected_images
  type: File[]
  outputBinding:
    glob: out/*.fits
- id: metadata
  type: File[]
  outputBinding:
    glob: out/*.json

baseCommand:
- reproject.py
arguments:
- position: 2
  valueFrom: out

hints:
- class: DockerRequirement
  dockerPull: git.astron.nl:5000/ssw-ksp/solar-im-compressing:latest

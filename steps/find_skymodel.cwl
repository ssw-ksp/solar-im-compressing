#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InlineJavascriptRequirement
- class: InitialWorkDirRequirement
  listing:
  - entryname: script.py
    entry: |
      import sys
      from casacore.tables import table
      import os
      CAL_MAP = {
          'CasA': 'CasA_4_patch',
          'VirA': 'VirA_4_patch',
          'Tau': 'TauAGG',
          'Taurus-A': 'TauAGG',
          'Cyg': 'CygAGG',
          '3C380': '3c380',
          '3C295': '3c295'
      }

      skymodel_path = {
        "3C380": "/usr/local/share/linc/skymodels/3C380_8h_SH.skymodel",
        "3C295": "/usr/local/share/linc/skymodels/3C295-sweijen.skymodel"
      }

      ms_in = table(sys.argv[1] + '/POINTING', ack=False)
      target_name = ""
      if(ms_in.nrows() !=0):
        target_name = ms_in[0]['NAME']
      else:
        ms_in = table(sys.argv[1] + '/OBSERVATION', ack=False)
        target_name = ms_in[0]['LOFAR_TARGET'][0]

      print(CAL_MAP[target_name])
      if target_name not in skymodel_path:
        os.system('cp /usr/local/var/data/skymodel/LBA.skymodel $PWD/calibrator.skymodel')
      else:
        os.system(f'cp {skymodel_path[target_name]} $PWD/calibrator.skymodel')

inputs:
- id: msin
  type: Directory
  inputBinding:
    position: 1

outputs:
- id: skymodel_file
  type: File
  outputBinding:
    glob: calibrator.skymodel
- id: target_source
  type: string
  outputBinding:
    glob: target_source
    outputEval: $(self[0].contents.trim())
    loadContents: true
stdout: target_source

baseCommand:
- python3
- script.py

hints:
  DockerRequirement:
    dockerPull: git.astron.nl:5000/ssw-ksp/solar-im-compressing:latest

#!/usr/bin/env python3
import h5py
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('agg')
import numpy
from argparse import ArgumentParser
import logging
import numpy as np
import astropy.time as atime
import os
import matplotlib.dates as mdates

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.INFO)


def parse_args():
    parser = ArgumentParser(description='Interpolate calibration solutions')
    parser.add_argument('input', help='input h5 parm dataset')
    parser.add_argument('out_path', help='output path')
    parser.add_argument('--solset', default='sol001')
    return parser.parse_args()


def read_sol_set(filename):
    return h5py.File(filename, 'r')


def plot_solutions(inset, outpath, solset):
    antennas = [k.decode() for k in inset['/sol000/amplitude000/ant']]
    frequencies = inset['/sol000/amplitude000/freq'][:] / 1.e6
    polarizations = [k.decode() for k in inset['/sol000/amplitude000/pol'][:]]
    times = inset['/sol000/amplitude000/time'][:]

    date_format = mdates.DateFormatter('%H:%M')

    # Deal with time conversion
    DAY_IN_SEC = 24 * 60 * 60
    times = atime.Time(times / DAY_IN_SEC, format='mjd').datetime
    os.makedirs(outpath, exist_ok=True)
    for ant_idx, antenna in enumerate(antennas):
        logging.info('Plotting solutions for antenna %s', antenna)
        for frequency_idx, frequency in enumerate(frequencies):
            for polarization_idx, polarization in enumerate(polarizations):

                amplitudes_full = inset['/sol000/amplitude000/val']
                amplitudes_fit = inset[f'/{solset}/amplitude000/val']
                phase_full = inset['/sol000/phase000/val']
                phase_fit = inset[f'/{solset}/phase000/val']
                fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
                title = f'ANTENNA {antenna} FREQ {frequency:.2f} POL {polarization}'
                fname = title.replace(' ', '_') + '.png'
                ax1.title.set_text(title)
                ax1.plot(times, amplitudes_full[:, frequency_idx, ant_idx, polarization_idx], color='black',
                         label='original')
                ax1.plot(times, amplitudes_fit[:, frequency_idx, ant_idx, polarization_idx], color='red', label='fit')
                ax1.legend()
                ax2.set_ylim([-3.15, 3.15])
                ax2.plot(times, phase_full[:, frequency_idx, ant_idx, polarization_idx], color='black',
                         label='original')
                ax2.plot(times, phase_fit[:, frequency_idx, ant_idx, polarization_idx], color='red',
                         label='fit')
                ax2.xaxis.set_major_formatter(date_format)

                for tick in ax2.get_xticklabels():
                    tick.set_rotation(45)
                plt.minorticks_on()
                ax1.tick_params(which='major', width=2, length=7)
                ax2.tick_params(which='major', width=2, length=7)
                plt.savefig(os.path.join(outpath, fname))
                plt.close(fig)


def main():
    args = parse_args()
    logging.info('Input dataset %s', args.input)
    h5parm = read_sol_set(args.input)
    plot_solutions(h5parm, args.out_path, solset=args.solset)


if __name__ == '__main__':
    main()

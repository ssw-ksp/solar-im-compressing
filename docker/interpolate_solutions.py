#!/usr/bin/env python3
import logging
from argparse import ArgumentParser

import h5py
import numpy
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RationalQuadratic
from sklearn.gaussian_process.kernels import WhiteKernel

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                    level=logging.DEBUG)


def parse_args():
    parser = ArgumentParser(description='Interpolate calibration solutions')
    parser.add_argument('input', help='input h5 parm dataset')
    parser.add_argument('in_solset', help='input solset')
    parser.add_argument('out_solset', help='output solset')
    parser.add_argument('--interpolation_type', choices=['average', 'linear', 'gaussian'],
                        default='average')
    return parser.parse_args()


def read_sol_set(solset_path):
    return h5py.File(solset_path, mode='r+')


def copy_solset(parmdb, in_solset, out_solset):
    if out_solset in parmdb:
        del parmdb[out_solset]
    parmdb.copy(in_solset, out_solset)


def moving_average(signal, window_size):
    filtered = np.convolve(signal, np.ones(window_size) / window_size, mode='same')
    return filtered


def moving_std(signal, window_size):
    filtered = moving_average(signal, window_size)
    moving_std = (signal - filtered) ** 2
    return moving_average(moving_std, window_size)


from joblib import parallel_backend, Parallel, delayed


def weight_from_data(dataset, weights, average_window_size, antenna_idx, frequency_idx,
                     polarization_idx):
    std_amplitude = moving_std(
        dataset[:, frequency_idx, antenna_idx, polarization_idx],
        average_window_size)
    weights[:, frequency_idx, antenna_idx, polarization_idx] = (
                                                                       std_amplitude.max() - std_amplitude) / std_amplitude.max()


def compute_weight(amplitudes, average_window_size):
    antennas = amplitudes['ant']
    amplitude_sol = amplitudes['val'][:]

    weights = numpy.zeros_like(amplitude_sol)

    for antenna_idx in range(amplitude_sol.shape[2]):
        logging.info('Computing weights for antenna %s', antennas[antenna_idx].decode())
        for polarization_idx in range(amplitude_sol.shape[3]):
            with parallel_backend('threading', n_jobs=2):
                with Parallel(n_jobs=5) as parallel:
                    parallel(delayed(weight_from_data)(amplitude_sol, weights,
                                                       average_window_size, antenna_idx,
                                                       frequency_idx, polarization_idx)
                             for frequency_idx in
                             range(amplitude_sol.shape[1]))

    amplitudes['weight'][:] = weights[:]
    return weights


def gaussian_regressor(times, signal, weight, fake=False):
    # Thanks to Alexander Kutkin
    kernel = 1.0 * RationalQuadratic(
        length_scale=1.0
    ) + WhiteKernel(0.01)
    timestamps = times.reshape(-1, 1)

    gpr = GaussianProcessRegressor(kernel=kernel, alpha=numpy.pi * (1 - weight[::2]))
    gpr.fit(timestamps[::2], signal[::2])
    y_mean, y_std = gpr.predict(timestamps, return_std=True)
    return y_mean, y_std


def linear_regressor(times, signal, weight):
    result = numpy.zeros_like(signal)
    std = numpy.zeros_like(signal)

    mask = weight < weight.min() + weight.std() / 2
    result[:] = signal[mask].mean()
    return result, std


def average_regressor(times, signal, weight):
    result = numpy.zeros_like(signal)
    std = numpy.zeros_like(signal) + 1

    result[:] = numpy.average(signal, weights=weight)
    return result, std


def solutions_from_data(sol_in, weight_in, times, frequency, antenna_idx,
                        polarization_idx,
                        frequency_idx, interpolation_type='average'):
    logging.info('Processing frequency %.2f', frequency[frequency_idx])
    data = sol_in[:, frequency_idx, antenna_idx, polarization_idx]
    weights = weight_in[:, frequency_idx, antenna_idx, polarization_idx]
    if interpolation_type == 'linear':
        return linear_regressor(times, data, weights)
    elif interpolation_type == 'average':
        return average_regressor(times, data, weights)
    else:
        return gaussian_regressor(times, data, weights)


def fit_solutions(phase_set, weight, interpolation_type):
    antennas = phase_set['ant']
    phase_sol = phase_set['val'][:]
    times = phase_set['time'][:]
    frequency = phase_set['freq'][:] / 1.e6
    phase_interp = numpy.zeros_like(phase_sol)
    weights_out = numpy.zeros_like(phase_sol)

    for antenna_idx in range(phase_sol.shape[2]):

        for polarization_idx in range(phase_sol.shape[3]):
            logging.info('Fitting phase solutions for antenna %s pol %s',
                         antennas[antenna_idx].decode(),
                         polarization_idx)

            with parallel_backend('loky', n_jobs=5):
                with Parallel(n_jobs=5) as parallel:
                    stuff = parallel(
                        delayed(solutions_from_data)(phase_sol, weight, times,
                                                     frequency,
                                                     antenna_idx,
                                                     polarization_idx,
                                                     frequency_idx,
                                                     interpolation_type=
                                                     interpolation_type)
                        for frequency_idx in
                        range(phase_sol.shape[1]))

                    for i in range(len(stuff)):
                        phase_interp[:, i, antenna_idx, polarization_idx] = stuff[i][0][
                                                                            :]
                        weights_out[:, i, antenna_idx, polarization_idx] = stuff[i][1][
                                                                           :]

    phase_set['val'][:] = phase_interp[:]
    phase_set['weight'][:] = weights_out[:]


def fit_solset(parmdb, solset, interpolation_type):
    amplitude = parmdb[solset + '/amplitude000']
    phase = parmdb[solset + '/phase000']
    weights = compute_weight(amplitude, 50)
    fit_solutions(phase, weights, interpolation_type)
    fit_solutions(amplitude, weights, interpolation_type)


def main():
    args = parse_args()
    logging.info('Input dataset %s: %s', args.input, args.in_solset)
    h5parm = read_sol_set(args.input)

    copy_solset(h5parm, args.in_solset, args.out_solset)
    fit_solset(h5parm, args.out_solset, args.interpolation_type)


if __name__ == '__main__':
    main()

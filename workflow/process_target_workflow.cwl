#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement
- class: SubworkflowFeatureRequirement

inputs:
- id: surls
  type: string[]
- id: solutions
  type: string

outputs:
- id: images
  type:
    type: array
    items:
      type: array
      items: File
  outputSource: imaging/images
- id: psf
  type:
    type: array
    items:
      type: array
      items: File
  outputSource: imaging/psf
- id: reprojected
  type: File
  outputSource: compress_reprojected/compressed
- id: metadata
  type: File
  outputSource: compress_metadata/compressed
- id: preview_video
  type:
    type: array
    items: File
  outputSource: imaging/preview_video

steps:
- id: fetch_solutions
  in:
  - id: surl_link
    source: solutions
  run: ../steps/fetch_solutions.cwl
  out:
  - id: solution
- id: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter: surl_link
  run: ../steps/fetch_data.cwl
  out:
  - uncompressed
- id: unflag
  in:
  - id: msin
    source: fetch_data/uncompressed
  scatter: msin
  run: ../steps/unflag.cwl
  out:
  - msout
- id: imaging
  in:
  - id: msin
    source: unflag/msout
  - id: solutions
    source: fetch_solutions/solution
  run: image_workflow.cwl
  out:
  - id: images
  - id: psf
  - id: reprojected
  - id: preview_video
  - id: metadata
- id: flatten_metadata
  in:
  - id: nested_file_list
    source: imaging/metadata
  run: ../steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: compress_metadata
  in:
  - id: files
    source: flatten_metadata/flattened_file_list
  - id: type
    valueFrom: metadata
  run: ../steps/compress.cwl
  out:
  - compressed
- id: flatten_images
  in:
  - id: nested_file_list
    source: imaging/images
  run: ../steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: compress_images
  in:
  - id: files
    source: flatten_images/flattened_file_list
  - id: type
    valueFrom: images
  run: ../steps/compress.cwl
  out:
  - compressed
- id: flatten_reprojected
  in:
  - id: nested_file_list
    source: imaging/reprojected
  run: ../steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: compress_reprojected
  in:
  - id: files
    source: flatten_reprojected/flattened_file_list
  - id: type
    valueFrom: reprojected
  run: ../steps/compress.cwl
  out:
  - compressed

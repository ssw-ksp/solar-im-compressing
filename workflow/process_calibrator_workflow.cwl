#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement
- class: SubworkflowFeatureRequirement

inputs:
- id: surls
  type: string[]

outputs:
- id: solutions
  type: File
  outputSource: calibration/solutions
- id: plots
  type: File
  outputSource: compress_inspect_plots/compressed

steps:
- id: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter: surl_link
  run: ../steps/fetch_data.cwl
  out:
  - id: uncompressed
- id: unflag
  in:
  - id: msin
    source: fetch_data/uncompressed
  scatter: msin
  run: ../steps/unflag.cwl
  out:
  - msout
- id: calibration
  in:
  - id: msin
    source: unflag/msout
  run: calibrator_workflow.cwl
  out:
  - id: solutions
  - id: inspect_plots
- id: compress_inspect_plots
  in:
  - id: files
    source: calibration/inspect_plots
  - id: type
    valueFrom: inspect-plots
  run: ../steps/compress.cwl
  out:
  - compressed

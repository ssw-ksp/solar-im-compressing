import logging
import os
import re
import urllib.parse
from argparse import ArgumentParser
from configparser import ConfigParser
from functools import reduce
from typing import Union, List, Dict

import pandas
import requests
from sqlalchemy import create_engine

CONNECTION_STRING = 'oracle+cx_oracle://{user}:{password}@db.lofar.target.rug.nl/?service_name=db.lofar.target.rug.nl'
DEFAULT_CONFIG_PATHS = ['~/.config/solar_processing/config.ini']
logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                    level=logging.DEBUG)

logger = logging.getLogger('Solar query LTA')

QUERY_STR_LBA = '''
SELECT FO.OBJECT_ID AS OBJECT_ID,
       FO.FILESIZE AS FILESIZE,
       ST.OBS_ID as OBS_ID,
       FO."+PROJECT" AS PROJECT,
       STR.PRIMARY_URL AS PRIMARY_URL FROM AWOPER.FILEOBJECT FO,
    AWOPER.STORAGETICKETRESOURCE STR,
    AWOPER.STORAGETICKET ST,
    AWOPER.CORRELATEDDATAPRODUCT CR
       WHERE FO.STORAGE_TICKET_RESOURCE=STR.OBJECT_ID AND
             CR.STORAGE_TICKET = ST.OBJECT_ID AND
             ST.OBJECT_ID = STR.TICKET AND
             OBS_ID = '{}'
'''

QUERY_STR_HBA = '''
SELECT FO.OBJECT_ID AS OBJECT_ID,
       FO.FILESIZE AS FILESIZE,
       ST.OBS_ID as OBS_ID,
       FO."+PROJECT" AS PROJECT,
       STR.PRIMARY_URL AS PRIMARY_URL FROM AWOPER.FILEOBJECT FO,
    AWOPER.STORAGETICKETRESOURCE STR,
    AWOPER.STORAGETICKET ST,
    AWOPER.CORRELATEDDATAPRODUCT CR
       WHERE FO.STORAGE_TICKET_RESOURCE=STR.OBJECT_ID AND
             CR.STORAGE_TICKET = ST.OBJECT_ID AND
             ST.OBJECT_ID = STR.TICKET AND
             OBS_ID = '{}'
'''
def read_user_credentials(file_paths: Union[str, List[str]]) -> Dict:
    file_paths = list(map(os.path.expanduser, map(os.path.expandvars, file_paths)))

    parser = ConfigParser()
    config_files = parser.read(file_paths)
    if len(config_files) == 0:
        raise Exception('Missing configuration files')
    credentials = {'lta_user': parser['global']['database_user'],
                   'lta_password': parser['global']['database_password'],
                   'atdb_url': parser['atdb']['url'],
                   'atdb_token': parser['atdb']['token']
                   }
    return credentials


def parse_surl_for_info(surl):
    logging.debug('Parsing surl %s', surl)
    parsed = urllib.parse.urlparse(surl)
    site = parsed.hostname
    path = parsed.path
    pattern = r'^.*/projects\/(?P<project>.*)\/(?P<sas_id>\w\d*)\/'
    groups = re.match(pattern, path).groupdict()

    result = dict()
    for key, value in groups.items():
        result[key] = value
    return site, result


def create_db_engine(user: str, password: str):
    connection_string: str = CONNECTION_STRING.format(
        user=urllib.parse.quote_plus(user),
        password=urllib.parse.quote_plus(password))

    return create_engine(connection_string)


def find_surl_and_size_per_sasid(engine, sasid, type='LBA'):
    if type.upper() == 'LBA':
        result_df = pandas.read_sql_query(QUERY_STR_LBA.format(sasid), engine)
    elif type.upper() == 'HBA':
        result_df = pandas.read_sql_query(QUERY_STR_HBA.format(sasid), engine)
    else:
        raise ValueError(f'Type {type} unrecognized')
    return result_df


def parse_args():
    parser = ArgumentParser(description='Utility to specify task on ATDB')
    parser.add_argument('--config_file', help='ATDB configuration file')
    parser.add_argument('--sample_size', default=244)
    parser.add_argument('--filter', default='')
    subparsers = parser.add_subparsers(help='mode', dest='mode')
    subparser = subparsers.add_parser('lba_cl')
    subparser.add_argument('sas_ids', nargs='+', help='SAS_IDS')
    subparser_submit = subparsers.add_parser('submit')
    subparser_submit.add_argument('filter')
    subparser_delete = subparsers.add_parser('delete')
    subparser_delete.add_argument('filter')

    subparser_hba = subparsers.add_parser('hba_cl')
    subparser_hba.add_argument('--calibrator', nargs='+', help='CALIBRATOR SAS_IDS', required=True)
    subparser_hba.add_argument('--target', nargs='+', help='TARGET SAS_IDS', required=True)
    subparser_hba.add_argument('--use-linc', action='store_true')

    return parser.parse_args()


def prepare_atdb_session(url, token):
    session = requests.session()
    session.url = url.rstrip('/')
    session.headers['Authorization'] = f'Token {token}'
    return session


def create_payload_from_file_obj(common_spec, file_objs):
    process_size = reduce(lambda s, x: s + x['size'], file_objs, 0)
    payload = {**common_spec}
    payload['inputs'] = file_objs
    payload['size_to_process'] = process_size
    return payload


def create_task_in_atdb(atdb_session: requests.Session, payload):
    response = atdb_session.post(atdb_session.url.rstrip('/') + '/tasks/', json=payload)
    if response.ok:
        return response.json()['id']
    else:
        raise Exception(response.reason)


def create_tasks_from_content_lba(atdb_session, sas_id_contents, filter_str=None,
                                  sample_size=10):
    surls = sas_id_contents['primary_url']
    file_sizes = sas_id_contents['filesize']
    print('Found surls', surls[0])
    site, base_specification = parse_surl_for_info(surls[0])
    base_specification['filter'] = filter_str
    base_specification['task_type'] = 'regular'
    base_specification['new_status'] = 'defining'

    file_objs = [{'class': 'File', 'surl': surl, 'site': site, 'size': file_size} for
                 surl, file_size in
                 zip(surls, file_sizes)]

    target_surl = list(filter(lambda file_obj: 'SAP000' in file_obj['surl'], file_objs))
    calibrator_surl = list(
        filter(lambda file_obj: 'SAP001' in file_obj['surl'], file_objs))

    target_surl = target_surl
    calibrator_surl = calibrator_surl
    calibrator_payload, target_payload = create_payload_from_file_obj(
        base_specification, calibrator_surl), \
        create_payload_from_file_obj(base_specification, target_surl)

    calibrator_payload['new_workflow_uri'] = 'solar_calibrator_imaging'
    target_payload['new_workflow_uri'] = 'solar_target_imaging'
    task_id_calibrator = create_task_in_atdb(atdb_session, calibrator_payload)
    target_payload['predecessor'] = int(task_id_calibrator)

    task_id_target = create_task_in_atdb(atdb_session, target_payload)
    return task_id_calibrator, task_id_target


def create_tasks_from_content_hba(atdb_session, calibrator_sas_id_contents,
                                  target_sas_id_contents, filter_str=None,
                                  use_linc=False,
                                  sample_size=10):
    calibrator_surls = calibrator_sas_id_contents['primary_url']
    target_surls = target_sas_id_contents['primary_url']

    cal_file_sizes = calibrator_sas_id_contents['filesize']
    tar_file_sizes = target_sas_id_contents['filesize']

    site, cal_base_specification = parse_surl_for_info(calibrator_surls[0])
    site, tar_base_specification = parse_surl_for_info(target_surls[0])
    for base_specification in [cal_base_specification, tar_base_specification]:
        base_specification['filter'] = filter_str
        base_specification['task_type'] = 'regular'
        base_specification['new_status'] = 'defining'

    calibrator_surl = [{'class': 'File', 'surl': surl, 'site': site, 'size': file_size} for
                 surl, file_size in
                 zip(calibrator_surls, cal_file_sizes)]
    target_surl = [{'class': 'File', 'surl': surl, 'site': site, 'size': file_size} for
                 surl, file_size in
                 zip(target_surls, tar_file_sizes)]
    
    calibrator_payload, target_payload = create_payload_from_file_obj(
        cal_base_specification, calibrator_surl), \
        create_payload_from_file_obj(tar_base_specification, target_surl)

    w_cal = 'linc_calibrator_v4' if use_linc else 'solar_calibrator_imaging'
    w_target = 'linc_target_v4' if use_linc else 'solar_target_imaging'

    calibrator_payload['new_workflow_uri'] = w_cal
    target_payload['new_workflow_uri'] = w_target
    task_id_calibrator = create_task_in_atdb(atdb_session, calibrator_payload)
    target_payload['predecessor'] = int(task_id_calibrator)

    task_id_target = create_task_in_atdb(atdb_session, target_payload)
    return task_id_calibrator, task_id_target


def specify_solar_pipeline_from_sas_ids_lba(sas_ids, filter_str=''):
    credentials = read_user_credentials(DEFAULT_CONFIG_PATHS)
    engine = create_db_engine(credentials['lta_user'], credentials['lta_password'])
    atdb = prepare_atdb_session(credentials['atdb_url'], credentials['atdb_token'])
    for sas_id in sas_ids:
        sas_id_contents = find_surl_and_size_per_sasid(engine, sas_id)
        task_id_cal, task_id_target = create_tasks_from_content_lba(atdb,
                                                                    sas_id_contents,
                                                                    filter_str=filter_str)
        print('Created ', task_id_cal, task_id_target)


def specify_solar_pipeline_from_sas_ids_hba(calibrators_sas_ids, targets_sas_ids,
                                            filter_str='', use_linc = False):
    credentials = read_user_credentials(DEFAULT_CONFIG_PATHS)
    engine = create_db_engine(credentials['lta_user'], credentials['lta_password'])
    atdb = prepare_atdb_session(credentials['atdb_url'], credentials['atdb_token'])
    for calibrator, target in zip(calibrators_sas_ids, targets_sas_ids):
        cal_sas_id_contents = find_surl_and_size_per_sasid(engine, calibrator, type='HBA')
        tar_sas_id_contents = find_surl_and_size_per_sasid(engine, target, type='HBA')
        task_id_cal, task_id_target = create_tasks_from_content_hba(atdb,
                                                                    cal_sas_id_contents,
                                                                    tar_sas_id_contents,
                                                                    filter_str=filter_str,
                                                                    use_linc=use_linc)
        print('Created ', task_id_cal, task_id_target)


def get_tasks_by_filter(atdb_session: requests.Session, filter_str):
    response = atdb_session.get(atdb_session.url + '/tasks/',
                                params={'filter': filter_str, 'status': 'defining'})
    if response.ok:
        return [result['id'] for result in response.json()['results']]
    else:
        raise Exception(response.reason)


def has_task_predecessor(atdb_session, task_id):
    response = atdb_session.get(atdb_session.url + f'/tasks/{task_id}/')
    if response.ok:
        return response.json()['predecessor'] is not None
    else:
        raise Exception(response.reason)


def set_task_to_defined(atdb_session, task_id):
    response = atdb_session.get(atdb_session.url + f'/tasks/{task_id}/')
    task_before = response.json()
    task_before.pop('status')
    task_before['new_status'] = 'defined'

    response = atdb_session.patch(atdb_session.url + f'/tasks/{task_id}/',
                                  {'new_status': 'defined',
                                   'resume': True})
    if response.ok:
        print('all good for', task_id)
    else:
        raise Exception(response.reason)


def submit_tasks(filter_str):
    credentials = read_user_credentials(DEFAULT_CONFIG_PATHS)
    atdb = prepare_atdb_session(credentials['atdb_url'], credentials['atdb_token'])
    tasks = get_tasks_by_filter(atdb, filter_str)
    for task_id in tasks:
        set_task_to_defined(atdb, task_id)


def delete_task(atdb_session, task_id):
    response = atdb_session.delete(atdb_session.url + f'/tasks/{task_id}/')
    if response.ok:
        print('deleted', task_id)
    else:
        raise Exception(response.reason)


def delete_tasks(filter_str):
    credentials = read_user_credentials(DEFAULT_CONFIG_PATHS)
    atdb = prepare_atdb_session(credentials['atdb_url'], credentials['atdb_token'])
    tasks = get_tasks_by_filter(atdb, filter_str)
    for task_id in tasks:
        delete_task(atdb, task_id)


def main():
    args = parse_args()
    if args.mode == 'lba_cl':
        specify_solar_pipeline_from_sas_ids_lba(args.sas_ids, filter_str=args.filter)
    if args.mode == 'hba_cl':
        specify_solar_pipeline_from_sas_ids_hba(args.calibrator, args.target,
                                                filter_str=args.filter,
                                                use_linc=args.use_linc)
    elif args.mode == 'submit':
        submit_tasks(args.filter)
    elif args.mode == 'delete':
        delete_tasks(args.filter)


if __name__ == '__main__':
    main()
